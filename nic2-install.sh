#!/bin/sh

mkdir ~/ifcfg-backup 2> /dev/null

ls -l /etc/sysconfig/network-scripts/ | grep ifcfg-e

read -p "What is the name of the second NIC (example: enp1001)?" NIC

cp /etc/sysconfig/network-scripts/ifcfg-$NIC ~/ifcfg-backup/

IFCFG="/tmp/ifcfg-$NIC"
#IFCFG="test2"

echo "" > $IFCFG

echo "TYPE=\"Ethernet\"
BOOTPROTO=\"none\"
DEFROUTE=\"no\"
IPV4_FAILURE_FATAL=\"no\"
IPV6INIT=\"no\"
NAME=\"$NIC\"" >> $IFCFG

DESC=`dmesg | grep $NIC | grep "Link status is: 1" | sed 's/.*] \([0-9a-z_]\+\) .*/\1/g'`
HWADDR=`dmesg | grep $DESC | grep eth0 | sed 's/.* \([0-9a-z]*:[0-9a-z]*:[0-9a-z]*:[0-9a-z]*:[0-9a-z]*:[0-9a-z]*\)$/\1/g'`

UUID=`grep UUID ~/ifcfg-backup/ifcfg-$NIC | sed 's/UUID=//g'`

echo "UUID=$UUID
DEVICE=\"$NIC\"
ONBOOT=\"yes\"
HWADDR=\"$HWADDR\"" >> $IFCFG

read -p "What is the internal IP address?" IPADDR

echo "IPADDR=\"$IPADDR\"
PREFIX=\"24\"" >> $IFCFG

read -p "What is the domain name (example: home, google.com)?" DOMAIN

echo "DOMAIN=\"$DOMAIN\"
ZONE=\"trusted\"" >> $IFCFG

cp $IFCFG /etc/sysconfig/network-scripts/
rm $IFCFG

systemctl restart network

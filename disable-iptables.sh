#!/bin/sh

for node in $@
do
	ssh root@$node 'service iptables save && service iptables stop && chkconfig iptables off'
done
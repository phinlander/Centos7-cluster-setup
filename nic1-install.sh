#!/bin/sh

mkdir ~/ifcfg-backup 2> /dev/null

cp /etc/sysconfig/network-scripts/ifcfg-eno1 ~/ifcfg-backup/

IFCFG="/tmp/ifcfg-eno1"
#IFCFG="test1"

echo "" > $IFCFG

echo "TYPE=\"Ethernet\"
BOOTPROTO=\"none\"
DEFROUTE=\"yes\"
IPV4_FAILURE_FATAL=\"yes\"
IPV6INIT=\"no\"
NAME=\"eno1\"" >> $IFCFG

UUID=`grep UUID ~/ifcfg-backup/ifcfg-eno1 | sed 's/UUID=//g'`

echo "UUID=$UUID
DEVICE=\"eno1\"
ONBOOT=\"yes\"" >> $IFCFG

HWADDR=`grep HWADDR ~/ifcfg-backup/ifcfg-eno1 | sed 's/HWADDR=//g'`

echo "HWADDR=$HWADDR" >> $IFCFG

read -p "What is the provided IP address?" IPADDR

echo "IPADDR=\"$IPADDR\"
PREFIX=\"26\"" >> $IFCFG

read -p "What is the netmask?" NETMASK

echo "NETMASK=\"$NETMASK\"" >> $IFCFG


read -p "What is the Gateway?" GATEWAY

echo "GATEWAY=\"$GATEWAY\"" >> $IFCFG

read -p "What is your machine's Hostname?" HOSTNAME

echo "NETWORKING=\"yes\"
HOSTNAME=\"$HOSTNAME\"
GATEWAY=\"$GATEWAY\"" > /etc/sysconfig/network

echo "$HOSTNAME" > /etc/hostname

echo "#DNS1=\"127.0.0.1\"" >> $IFCFG

read -p "What is the first DNS?" DNS1

echo "DNS1=\"$DNS1\"" >> $IFCFG

read -p "What is the second DNS?" DNS2

echo "DNS2=\"$DNS2\"" >> $IFCFG

read -p "What is the domain name (example: home, google.com)?" DOMAIN

echo "DOMAIN=\"$DOMAIN\"
NM_CONTROLLED=\"yes\"" >> $IFCFG

cp $IFCFG /etc/sysconfig/network-scripts/
rm $IFCFG

systemctl restart network

#!/bin/sh

rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
yum -y install epel-release http://yum.theforeman.org/releases/1.8/el7/x86_64/foreman-release.rpm
yum -y install foreman-installer
foreman-installer

puppet agent --test

cp -r environments/* /etc/puppet/environments/

TMP="/tmp/ifcfg-eno1"
IFCFG="/etc/sysconfig/network-scripts/ifcfg-eno1"
cp $IFCFG $TMP

sed -i 's/^DNS/#DNS/g' $TMP && sed -i 's/#DNS1=\"127/DNS1=\"127/g' $TMP && mv $TMP $IFCFG
#!/bin/sh

for node in $@
do
	ssh root@$node 'service ntpd stop && ntpdate -s time.nist.gov && service ntpd start && chkconfig --add ntpd && chkconfig ntpd on'
done

class master {
	$packages = ["nano","tmux","git","java-1.7.0-openjdk-devel.x86_64","rpm-build","make","wget","ant","ant-nodeps","gcc-c++","glibc-devel","atop","automake","tar"]
        package { $packages:
                ensure => installed,
        }

	exec { "swappiness":
                command => "sysctl -w vm.swappiness=0 && echo 'vm/swappiness=0'>>/etc/sysctl.conf",
                path    => ["/bin","/sbin","/usr/bin", "/usr/sbin"],
        }
     
}

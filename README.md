# Centos7 Cluster Setup Scripts
Scripts for full foreman install on CentOS7 including network setup. All scripts must be run as root.

## Necessary scripts

### NIC1-Install
Prompts for information for your __outward__ facing network interface card. Also sets up /etc/resolve.conf and /etc/hostname.

### Foreman-Install
Runs through the defaul Foreman setup for CentOS7  
Updates ifcfg-eno1 (NIC1) when complete, commenting out the original DNS servers and pointing to itself.

### NIC2-Install
Prompts for information for your __inward__ facing network interface card.

### Firewall-Install
Sets up your firewall for internal traffic and outward masquerading.

## Helpful scripts

### Disable-IPTables
This script takes a list of FQDN names (example: node1.network) of your nodes and disables IP tables. This is used for situations such as installing Cloudera manager.  
__Note:__ This will require a password for each node unless you set it up to work otherwise. Logs into nodes as root.

### Reboot
This script takes a list of FQDN names (example: node1.network) of your nodes and sends a reboot command.  
__Note:__ This will require a password for each node unless you set it up to work otherwise. Logs in to nodes as root.

### Restore
Takes the ifcfg-e* files from ~/ifcfg-backup and restores them to their original location.
